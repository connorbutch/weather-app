# internal-emf-example

This project is used in conjunction with a medium article (TODO link).
It shows how to use emf to create latency metric with a dimension for the version of the sdk, 
and an attribute for company id (which will have high cardinality).  

It then includes a postman collection that can be ran to generate traffic and emit metrics.

It then includes an example of how to query data from properties in emf (company id) using cloudwatch logs insights
to view the latency for a particular company (based on company id).

## Deployment
The first time you run the project, do the following
- authenticate with your aws account
- run this command

```
sam build && sam deploy --guided
``` 

If you want to make changes in the repo on your own, for every subsequent deploy, you can simply run
```
sam build && sam deploy
``` 

## Generating Traffic
install newman (if you don't already have it).

```
npm install -g newman
``` 

generate traffic against your api.  Please replace the <TODO your url here> with the url that is the output of the sam
deploy step taken above
```
cd postman
newman run weather_postman_collection.json --global-var "weatherUrl=<TODO your url output from sam output here>" -n 20
```

## Project status
This project is complete.  It exists to go along with the medium article, and has served it's purpose.