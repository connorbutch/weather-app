const { v4: generateUUID } = require('uuid');
const { createMetricsLogger, Unit } = require("aws-embedded-metrics");
const SLOW_COMPANY_ID = "90556f50-bed9-47be-a894-863a166ddf45";

exports.lambdaHandler = async (event, context) => {
    const metrics = createMetricsLogger();
    metrics.setNamespace("weather");
    const companyId = getCompanyId(event);
    console.log("Company id: " + companyId);
    const simulatedVersionNumber = getRandomIntInclusive(1,3);
    console.log("Version number: " + simulatedVersionNumber);
    const simulatedTimeToFetchWeatherData = getSimulatedLatencyToFetchWeatherDataInMs(companyId);
    console.log("Time to fetch: " + simulatedTimeToFetchWeatherData);
    metrics.setDimensions({ "weatherClientVersion": simulatedVersionNumber.toString() });
    metrics.putMetric("weatherRetrievalLatency", simulatedTimeToFetchWeatherData, Unit.Milliseconds);
    metrics.setProperty("companyId", companyId);
    await metrics.flush();
    return {
        'statusCode': 200,
        'body': JSON.stringify({
            temperatureInDegreesFahrenheit: 75
        })
    }
};

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getCompanyId(event){
    //in the real world, you would probably reject requests without a company id with a 400 (should be populated by lambda custom authorizer), but for the sake of this demo, we just generate a random one
    return event.headers?.['x-company-id'] === undefined ? generateUUID() : event.headers['x-company-id'];
}

function getSimulatedLatencyToFetchWeatherDataInMs(companyId){
    let simulatedLatencyInMs;
    if(SLOW_COMPANY_ID === companyId){
        simulatedLatencyInMs = generateNumbersInNormalDistribution(2500, 200);
    }else{
        simulatedLatencyInMs = generateNumbersInNormalDistribution(500, 300);
    }
    return simulatedLatencyInMs;
}

function generateNumbersInNormalDistribution(median, standardDistribution) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random();
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) return generateNumbersInNormalDistribution();
    return (.5 - num) * standardDistribution + median
}